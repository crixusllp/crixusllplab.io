class RadForm {
  constructor(form) {
    this.form = form;
    this.inputs = Array.from(this.form.querySelectorAll('input, textarea'));
    this.tooltips = [];
    this.requiredIndicators = [];
    this.returnButton = this.form.querySelector('.rad-form-feedback-return').addEventListener('click', () => this.returnToForm());

    this.initiateForm();
    this.checkForm();
  }

  initiateForm() {
    // this.addRequiredIndicators();
    window.addEventListener('resize', () => this.onResize());
  }

  addRequiredIndicators() {
    this.inputs.forEach(input => {
      if (input.required) {
        let indicator = document.createElement('DIV');
        indicator.classList.toggle('rad-form-required-indicator', true);
        this.requiredIndicators.push({indicator: indicator, relatedInput: input});
        this.form.appendChild(indicator);
      }
    });
    this.positionRequiredIndicators();
  }

  positionRequiredIndicators() {
    this.requiredIndicators.forEach(indicator => {
      indicator.indicator.style.left = `${indicator.relatedInput.offsetLeft + indicator.relatedInput.offsetWidth - indicator.indicator.offsetWidth}px`;
      indicator.indicator.style.top = `${indicator.relatedInput.offsetTop}px`;
    });
  }

  onResize() {
    this.positionRequiredIndicators();
  }

  checkForm() {
    this.checkInputs();
  }

  checkInputs() {
    this.inputs.forEach((input) => {
      if (input.dataset.errorMessage != null)  {
        this.addError(input);
      }
    });
  }

  addError(input) {
    input.classList.toggle('rad-form-invalid-input', true);
    input.addEventListener('focus', () => {this.removeError(input)});
    this.createTooltip(input);
  }

  removeError(input) {
    input.classList.toggle('rad-form-invalid-input', false);
    let relatedTooltip = this.tooltips.find((tooltip) => tooltip.input == input);
    relatedTooltip.tooltip.classList.toggle('rad-form-tooltip--hidden', true);
  }

  createTooltip(input) {
    let tooltip = document.createElement('DIV');
    tooltip.classList.add('rad-form-tooltip');
    tooltip.innerHTML = input.dataset.errorMessage;
    this.tooltips.push({tooltip: tooltip, input: input});
    this.form.appendChild(tooltip);

    tooltip.style.left = `${input.offsetLeft}px`;
    tooltip.style.top = `${input.offsetTop + input.getBoundingClientRect().height - 20}px`;
  }

  returnToForm() {
    this.form.classList.toggle('rad-form--error', false);
  }
}
